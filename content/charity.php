<div class="left_column">
  <div class="sanskritBG"></div>
</div>
<div class="right_column">
	<img class="section_hero" src="images/Philippa_pics/SECTION_HEROES/charity.jpg" width="700" height="313" />
    <h2>Clean the Ganges</h2>
    <p>You and find out more about the charity <a href="http://sankatmochan.tripod.com/" target="_blank">here</a>.</p>
    <h3>Tributes to Professor Veer Bhadra Mishra:</h3>
    <p><a href="http://www.thehindu.com/opinion/op-ed/warrior-for-a-river/article4513156.ece" target="_blank">Warrior for a river</a></p>
    <p><a href="http://articles.timesofindia.indiatimes.com/2013-03-14/varanasi/37714514_1_nrgba-clean-ganga-campaign-akhilesh-yadav" target="_blank">Mortal remains of Veer Bhadra Mishra consigned to flames</a></p>
    <p><a href="http://www.itbhuglobal.org/chronicle/archives/2013/03/sad_demise_of_p.php" target="_blank">Sad Demise of Professor Veer Bhadra Mishra</a></p>
    <p style="margin-top:40px"><a class="donate_btn" href="http://www.sankatmochanfoundationonline.org/contact.html" target="_blank">Donate here</a></p>
</div>
