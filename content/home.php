<div class="home_content">
  <div class="text_box">
  	<h1>Pravaha</h1>
    <h2><strong>Bringing the flow to life through Yoga and Film.</strong></h2>
  	<p>Pravaha's ambition is two fold. It aims to enhance your wellbeing, shedding light on yoga practise for the benefit of mind and body.  It also entertains and informs, as issues affecting our inner world are brought into focus through short, thought provoking films.</p>
    <p>Pravaha is a website created by Philippa Frisby.</p>
  	
  </div>
  <img class="hero_img" src="images/Philippa_pics/HomeHero.jpg" width="595" height="360" />
<div class="clear"></div>
  <div style="margin-top: 20px">
  	<div class="launch_box">
  		<a href="?page=articles"></a>
  		<h2>Articles</h2>
  		<img src="images/Philippa_pics/HomeLinksArticles.jpg" width="228" height="210" />
    </div>
  	<div class="launch_box">
  		<a href="?page=films"></a>
  		<h2>Films</h2>
    	<img src="images/Philippa_pics/HomeLinksFilm.jpg" width="228" height="210" /> 
    </div>
  	<div class="launch_box">
  		<a href="?page=yoga"></a>
  		<h2>Yoga</h2>
    	<img src="images/Philippa_pics/HomeLinksYoga.jpg" width="228" height="210" /> 
    </div>
  	<div class="launch_box last">
  		<a href="?page=charity"></a>
  		<h2>Charity</h2>
    	<img src="images/Philippa_pics/HomeLinksCharity.jpg" width="228" height="210" /> 
    </div>
  </div>
</div>
