<?php 
	$pathToImages = 'gallery/';
	$imagesArray = glob($pathToImages . '*.{jpg,jpeg,png,gif}', GLOB_BRACE);
	$imagesHtml ='';
	foreach($imagesArray as $imagePath){
		$imagesHtml .= "<img src='$imagePath' width='700' height='517' />";
	}
?>
<div class="gallery_container">

  <div class="cycle-slideshow" data-cycle-fx="scrollHorz" data-cycle-next=".cycle-next" data-cycle-prev=".cycle-prev" data-cycle-pause-on-hover="true" data-cycle-swipe="true">
    
  <?php echo $imagesHtml ?>
    </div>
  <div class="next_prev_btns">
    <div class="cycle-next"></div>
    <div class="cycle-prev"></div>
  </div>
</div>
<script type="text/javascript" src="js/jquery.cycle2.min.js"></script>
