<?php 
	require('details/details.php');
	
	$error_msg = "";
	if (isset($_POST['submit'])){
		$fromEmail = $_POST['fromEmail'];
		$emailMessage = $_POST['email_message'];
		setErrorMessage(testEmail($fromEmail, $emailMessage));
	}
	function testEmail($email, $mess){
		global $emailTo;
		if (!isset($email) || $email == '') return 'Please enter your email address';
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) return 'Please enter a valid email address';
		if (!isset($mess) || $mess == '') return 'Please enter a message';
		$headers = "From:" . $email;
		if (!mail($emailTo,"Email from Pravaha.co.uk",$mess,$headers)) return 'Error sending message';
		return 'Thank you - your message has been sent';
	}
	function setErrorMessage($mess){
		global $error_msg;
		$error_msg = ($mess == '')? '':'<p class="error_msg">• '.$mess.'</p>';
	}
?>
<div class="left_column">
  <div class="sanskritBG"></div>
</div>
<div class="right_column">
	<img class="section_hero" src="images/Philippa_pics/SECTION_HEROES/contact.jpg" width="700" height="313" />
    <h2>Contact</h2>
    <div class="email_form">
    <?php echo $error_msg ?>
      <form action="" method="post" name="email_form">
        <p>Your email address:<input class="fromEmail" name="fromEmail" type="text" value="<?php echo $fromEmail ?>" /></p>
        <p>Your message:</p>
        <textarea class="email_message" name="email_message" value="Enter your message" cols="" rows=""></textarea>
        <input class="send_btn" name="submit" type="submit" value="Send" />
      </form>
    </div>
</div>
