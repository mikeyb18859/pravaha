<div class="left_column">
  <div class="sanskritBG"></div>
</div>
<div class="right_column">
	<img class="section_hero" src="images/Philippa_pics/SECTION_HEROES/about.jpg" width="700" height="313" />
<h2>About Philippa</h2>
    <p>Philippa Frisby is an Iyengar Yoga Teacher, who makes short documentary films. She started practising Iyengar yoga in 1990 whilst at University. Although subsequently enjoying a successful career as a documentary film maker, her love of yoga stayed with her and she was drawn to undertake the demanding discipline of qualifying as an Iyengar Yoga teacher. She started teaching in 2001, and gained a Junior Intermediate certificate in the Iyengar system in 2008. She now teaches both in her small dedicated  home-based yoga studio and at The Yogic Path School in London, where she continues to study with her own teacher, the acclaimed yoga author and practitioner Mira Mehta.</p>

<p>A student of philosophy, Ayurveda and Sanskrit, Philippa also regularly travels to India for  her continuing professional development with  Senior Iyengar Teachers. An insightful and inspirational practitioner, her teaching has been described as: 'intuitive and compassionate but always challenging and enabling the student to go further.'</p>

<p>Continuing to use her skills as a film maker, Philippa is currently making short documentaries as part of an ongoing project.  Each film features a subject's quirky personal story: often funny, sometimes sad and always  moving, each film brings a fresh perspective to contemporary issues and ideas that affect us all.</p>

<p>Philippa is married with two children and lives in London.</p>

</div>
