<?php 
	$subTitle = 'Films';
	$subPath = '/films';
	$subItems = array(
		//'The Ganges' => 'ganges.html',
		'Films' => '',
		'The River - a visual poem' => 'theriver.html',
		'The Priest Scientist and the Holy River 1' => 'priestScientist1.html',
		'The Priest Scientist and the Holy River 2: The Gravity Pump' => 'priestScientist2.html',
		'The Priest Scientist and the Holy River 3: Goddess Mother' => 'priestScientist3.html'
	);
	
	include_once('templates/multi_page.php');
?>