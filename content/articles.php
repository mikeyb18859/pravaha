<?php 
	$subTitle = 'Articles';
	$subPath = '/articles';
	$subItems = array(
		'Articles' => '',
		/*'Swatcha Ganga' => 'swatchaGanga.html',*/
		'AUM the sacred syllable' => 'aum.html',
		'Yoga' => '',
		'The Spine' => 'theSpine.html',
		'Alignment' => 'alignment.html',
		'Meditation in Yoga!' => 'meditationInYoga.html',
		'Prayers and Chanting' => 'prayersAndChanting.html',
		'Ruminations or musings on Tadasana and Sirsasana' => 'tadasanaAndSirsasana.html',
		'Sanskrit Proverbs and Sayings' => 'sanscritProverbs.html',
		'Tirumalai Krishnamacharya' => 'tirumalaiKrishnamacharya.html'
	);
	
	include_once('templates/multi_page.php');
?>